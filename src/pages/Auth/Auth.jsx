import React, {useState} from 'react';
import {LoginForm} from "../../components/Auth/LoginForm";
import {AuthOptions} from "../../components/Auth/AuthOptions";
import {RegisterForm} from "../../components/Auth/RegisterForm";

import BackgroundAuth from '../../assets/jpg/background-auth.jpg';
import LogoNameWhite from '../../assets/png/logo-name-white.png';

export const Auth = () => {

    const [selectedForm, setSelectedForm] = useState('');

    const handleForm = () => {
        switch (selectedForm) {
            case 'login':
                return <LoginForm setSelectedForm={setSelectedForm}/>;
            case 'register':
                return <RegisterForm setSelectedForm={setSelectedForm}/>;
            default:
                return <AuthOptions setSelectedForm={setSelectedForm}/>;
        }
    }

    return (

        <div className="auth"
             style={{backgroundImage: `url(${BackgroundAuth})`}}>

            <div className="auth__dark"/>
            <div className="auth__box">
                <div className="auth__box-logo">
                    <img src={LogoNameWhite} alt="SongFy"/>
                </div>
                {handleForm()}
            </div>

        </div>

    )
};