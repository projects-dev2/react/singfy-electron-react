import React, {useEffect, useState} from 'react';
import {Link, withRouter} from "react-router-dom";
import {Icon, Menu} from "semantic-ui-react";
import {isUserAdmin} from "../../utils/ApiFirebase";
import {BasicModal} from "../../components/Modal/BasicModal";

export const MenuLeft = withRouter(({user, location}) => {

    const [activeMenu, setActiveMenu] = useState(location.pathname);
    const [userAdmin, setUserAdmin] = useState(false);

    // Modal
    const [showModal, setShowModal] = useState(false);
    const [titleModal, setTitleModal] = useState(null);
    const [contentModal, setContentModal] = useState(null);

    useEffect(() => {
        isUserAdmin(user.uid).then(resp => setUserAdmin(resp));
    }, [user]);

    useEffect(() => {
        setActiveMenu(location.pathname);
    }, [location]);

    const handleMenu = (e, menu) => {
        setActiveMenu(menu.to);
    };

    const handleModal = (type) => {
        switch (type) {
            case 'artist':
                setTitleModal('Nuevo Artista');
                setContentModal(<h2>FORM ARTIST</h2>);
                setShowModal(true);

                break;

            case 'song':
                setTitleModal('Nuevo Cancion');
                setContentModal(<h2>FORM SONG</h2>);
                setShowModal(true);

                break;

            default:
                setTitleModal(null)
                setContentModal(null)
                setShowModal(false);

                break;
        }
    };


    return (

        <>
            <Menu className="menu-left" vertical>

                <div className="top">

                    <Menu.Item as={Link}
                               to="/"
                               active={activeMenu === "/"}
                               onClick={handleMenu}>
                        <Icon name="home"/>Inicio
                    </Menu.Item>

                    <Menu.Item as={Link}
                               to="/artists"
                               active={activeMenu === "/artists"}
                               onClick={handleMenu}>
                        <Icon name="music"/>Artistas
                    </Menu.Item>

                </div>

                {
                    userAdmin && (

                        <div className="footer">

                            <Menu.Item onClick={() => handleModal('artist')}>
                                <Icon name="plus square outline"/>Nuevo Artista
                            </Menu.Item>

                            <Menu.Item onClick={() => handleModal('song')}>
                                <Icon name="plus square outline"/>Nuevo Canción
                            </Menu.Item>

                        </div>

                    )
                }

            </Menu>

            <BasicModal show={showModal} setShow={setShowModal} title={titleModal}>
                {contentModal}
            </BasicModal>
        </>

    )
});