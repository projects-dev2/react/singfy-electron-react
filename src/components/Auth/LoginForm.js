import React, {useState} from 'react';
import {toast} from "react-toastify";
import {validateFormLogin} from "../../utils/Validation";
import {Button, Form, Icon, Input} from "semantic-ui-react";
import {ResendVerification} from "./ResendVerification";
import {getAuth, signInWithEmailAndPassword} from "firebase/auth";

export const LoginForm = ({setSelectedForm}) => {

    const [showPassword, setShowPassword] = useState(false);
    const [dataForm, setDataForm] = useState({
        email: '',
        password: '',
    });
    const [errors, setErrors] = useState({});
    const [isLoading, setIsLoading] = useState(false);

    const [userActive, setUserActive] = useState(true);
    const [user, setUser] = useState(null);

    const handleSubmitLogin = () => {

        // to Validate
        const [errorsLogin, formOk] = validateFormLogin(dataForm);
        setErrors(errorsLogin);

        if (formOk) {
            setIsLoading(true);

            signInWithEmailAndPassword(getAuth(), dataForm.email, dataForm.password)
                .then(({user}) => {
                    setUser(user);
                    setUserActive(user.emailVerified);

                    if (!user.emailVerified) {
                        toast.warning('Verifica tu cuenta, mira tu correo', {theme: "colored"})
                    }
                    setIsLoading(false);
                })
                .catch((error) => {
                    handleCodeError(error.code);
                    setIsLoading(false);
                })
            // .finally(() => {
            // });
        }
    }

    const handleChange = (e) => {
        setDataForm({
            ...dataForm,
            [e.target.name]: e.target.value
        });
    }

    const handleCodeError = (code) => {
        switch (code) {
            case "auth/wrong-password":
                toast.warning("El usuario o la contraseña son incorrecto.", {theme: "colored"});
                break;
            case "auth/too-many-requests":
                toast.warning(
                    "Has enviado demasiadas solicitudes de reenvio de email de confirmacion en muy poco tiempo.", {theme: "colored"});
                break;
            case "auth/user-not-found":
                toast.warning("El usuario o la contraseña son incorrecto.", {theme: "colored"});
                break;
            default:
                break;
        }
    }

    return (

        <div className="login-form">
            <h1>Música para todos.</h1>

            <Form onSubmit={handleSubmitLogin} onChange={handleChange}>

                <Form.Field>
                    <Input
                        type="text" name="email"
                        placeholder="Correo Electrónico" icon="mail outline"
                        error={errors.email}
                    />

                    {
                        errors.email && (
                            <span className="error-text">Por favor, introduzca un correo válido.</span>
                        )
                    }
                </Form.Field>

                <Form.Field>
                    <Input
                        type={showPassword ? 'text' : 'password'}
                        name="password"
                        error={errors.password}
                        placeholder="Contraseña" icon={
                        showPassword ?
                            (<Icon name="eye slash outline" link onClick={() => setShowPassword(!showPassword)}/>)
                            :
                            (<Icon name="eye" link onClick={() => setShowPassword(!showPassword)}/>)
                    }
                    />
                    {
                        errors.password && (
                            <span className="error-text">La contraseña es requerida.</span>
                        )
                    }
                </Form.Field>

                <Button disabled={isLoading} loading={isLoading} type="submit">Iniciar Sesión</Button>

            </Form>

            {
                !userActive && (
                    <ResendVerification user={user} setIsLoading={setIsLoading} setUserActive={setUserActive}/>
                )
            }

            <div className="login-form__options">
                <p onClick={() => setSelectedForm(null)}>Volver</p>
                <p>¿No tienes cuenta? {" "}
                    <span onClick={() => setSelectedForm('register')}>Regístrate</span>
                </p>
            </div>

        </div>

    )
};