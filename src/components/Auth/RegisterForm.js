import React, {useState} from 'react';
import {Button, Icon, Form, Input} from "semantic-ui-react";
import {validateFormRegister} from "../../utils/Validation";
import {updateProfile, getAuth, createUserWithEmailAndPassword, sendEmailVerification} from 'firebase/auth';
import {toast} from "react-toastify";

export const RegisterForm = ({setSelectedForm}) => {

    const [formData, setFormData] = useState({
        email: '',
        password: '',
        username: '',
    });

    const [showPassword, setShowPassword] = useState(false);
    const [formError, setFormError] = useState({});
    const [isLoading, setIsLoading] = useState(false);

    const handleSubmit = () => {
        setFormError({});

        const [errors, formOk] = validateFormRegister(formData);
        setFormError(errors);

        if (formOk) {
            setIsLoading(true);

            createUserWithEmailAndPassword(getAuth(), formData.email, formData.password)
                .then(({user}) => {
                    // toast.success('Usuario registrado', {theme: "colored"});
                    setIsLoading(false);
                    changeUserName();
                    sendVerificationEmail();
                })
                .catch(error => {
                    console.error(error);
                    toast.error('Error en crear la cuenta', {theme: "colored"});
                })
                .finally(() => {
                    setIsLoading(false);
                    setSelectedForm('login');
                })
        }
    }

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        });
    }

    const changeUserName = () => {

        updateProfile(getAuth().currentUser, {
            displayName: formData.username
        })
            // .then(() => {
            //     // Profile updated!
            // })
            .catch((error) => {
                toast.error(`Error al asignar el nombre de usuario, ${error}`, {theme: "colored"});
            });
    }

    const sendVerificationEmail = () => {

        sendEmailVerification(getAuth().currentUser)
            .then(() => {
                // toast.success('Usuario registrado', {theme: "colored"});
                toast.success('Usuario registrado, Se ha enviado un email de verificación', {theme: "colored"});
            })
            .catch(() => {
                toast.error('Error al enviar email de verificación', {theme: "colored"});
            });
    }

    return (

        <div className="register-form">
            <h1>Empieza a escuchar con una cuenta de Songfy gratis.</h1>

            <Form onSubmit={handleSubmit} onChange={handleChange}>

                <Form.Field>
                    <Input
                        type="text" name="email"
                        placeholder="Correo Electrónico" icon="mail outline"
                        error={formError.email}
                    />
                    {
                        formError.email && (
                            <span className="error-text">Por favor, introduzca un correo válido.</span>
                        )
                    }
                </Form.Field>

                <Form.Field>
                    <Input
                        type={showPassword ? 'text' : 'password'}
                        error={formError.password}
                        name="password" placeholder="Contraseña" icon={
                        showPassword ?
                            (<Icon name="eye slash outline" link onClick={() => setShowPassword(!showPassword)}/>)
                            :
                            (<Icon name="eye" link onClick={() => setShowPassword(!showPassword)}/>)
                    }
                    />
                    {
                        formError.password && (
                            <span className="error-text">La contraseña debe ser al menos de 6 letras</span>
                        )
                    }
                </Form.Field>

                <Form.Field>
                    <Input
                        type="text" name="username"
                        placeholder="¿Como deberíamos llamarte" icon="user circle outline"
                        error={formError.username}
                    />
                    {
                        formError.username && (
                            <span className="error-text">Escriba algún Username</span>
                        )
                    }
                </Form.Field>

                <Button type="submit" loading={isLoading} disabled={isLoading}>Registrar</Button>

                <div className="register-form__options">
                    <p onClick={() => setSelectedForm(null)}>Volver</p>
                    <p>¡Ya tienes Songfy? <span onClick={() => setSelectedForm('login')}>Iniciar sesión</span></p>
                </div>

            </Form>

        </div>

    )
};