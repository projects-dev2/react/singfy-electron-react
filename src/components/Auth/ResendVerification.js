import React from 'react';
import {sendEmailVerification} from "firebase/auth";
import {toast} from "react-toastify";

export const ResendVerification = ({user, setIsLoading, setUserActive}) => {

    const handleResendVerificationEmail = () => {

        sendEmailVerification(user)
            .then(() => {
                toast.success('Se ha enviado un email de verificación', {theme: "colored"});
                setIsLoading(false);
                setUserActive(true);
            })
            .catch((error) => {
                console.error(error);
                toast.error(`Error al enviar email de verificación`, {theme: "colored"});
            })
        // .finally(() => {
        //
        // });
    }

    return (

        <div className="resend-verification-email">
            <p>
                Si no has recibido el email de verificación, puedes volver a solicitarlo, haciendo click {" "}
                <span onClick={handleResendVerificationEmail}>aquí.</span>
            </p>
        </div>

    )
};