import React from 'react';
import {Link, withRouter} from "react-router-dom";

import UserImage from '../../assets/png/user.png';
import {Icon, Image} from "semantic-ui-react";
import {logout} from "../../utils/ApiFirebase";

export const TopBar = withRouter(({user, setUser, history}) => {

    const handleGoBack = () => {
        history.goBack();
    };

    const handleLogout = () => {
        logout();
    };

    return (

        <div className="top-bar">

            <div className="top-bar__left">
                <Icon name="angle left" onClick={handleGoBack}/>
            </div>

            <div className="top-bar__right">
                <Link to="/settings">
                    <Image src={UserImage}/>
                    <p>{user.displayName}</p>
                </Link>

                <Icon name="power off" onClick={handleLogout}/>

            </div>

        </div>

    )
});


