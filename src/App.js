import {useState} from "react";

import {ToastContainer} from 'react-toastify';
import {getAuth, onAuthStateChanged, signOut} from 'firebase/auth';
import './utils/firebasej';
import {Auth} from "./pages/Auth/Auth";
import {LoggedLayouts} from "./layouts/LoggedLayouts";

function App() {

    const [user, setUser] = useState(null);
    const [isLoading, setIsLoading] = useState(true);

    onAuthStateChanged(getAuth(), async (userCurrent) => {

        if (!userCurrent?.emailVerified) {
            signOut(getAuth());
            setUser(null);
        } else {
            setUser(userCurrent)
        }
        setIsLoading(false);
    });

    if(isLoading){
        return null;
    }


    // useEffect(() => {
    //
    //     onAuthStateChanged(getAuth(), async (userCurrent) => {
    //
    //         if (userCurrent !== null) {
    //             setUser(userCurrent)
    //             setIsLoggedIn(true);
    //         } else {
    //             setUser(null);
    //             setIsLoggedIn(false);
    //         }
    //
    //     });
    //
    // }, [setIsLoggedIn,setUser]);


    return (
        <>
            {!user ? (<Auth/>) : (<LoggedLayouts user={user}/>)}

            <ToastContainer
                position="top-left" autoClose={6500}
                newestOnTop closeOnClick
                rtl={false} pauseOnFocusLoss
                draggable pauseOnHover
            />
        </>
    );
}

export default App;
