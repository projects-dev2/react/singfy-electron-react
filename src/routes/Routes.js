import React from 'react';
import {Switch, Route} from "react-router-dom";
import {Home} from "../pages/Dashboard/Home";
import {Artist} from "../pages/Dashboard/Artist";
import {Settings} from "../pages/Dashboard/Settings";

export const Routes = () => {
    return (

        <Switch>
            <Route path="/" exact component={Home}/>
            <Route path="/artists" exact component={Artist}/>
            <Route path="/settings" exact component={Settings}/>
        </Switch>

    )
};
