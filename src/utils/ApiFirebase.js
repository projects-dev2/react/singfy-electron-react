import {db} from "./firebasej";
import {doc, getDoc} from "firebase/firestore";
import {getAuth, signOut} from 'firebase/auth';


export async function isUserAdmin(uid) {

    const docRef = doc(db, "admins", uid);
    const docSnap = await getDoc(docRef);

    return docSnap.exists();

}

export async function logout() {
    await signOut(getAuth());
}