export function validateEmail(email) {

    //eslint-disable-next-line
    const validation = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    return validation.test(String(email).toLowerCase());
}

export function validateFormRegister(formData) {
    let errors = {};
    let formOk = true;

    if (!validateEmail(formData.email)) {
        errors.email = true;
        formOk = false;
    }

    if (formData.password.length < 6) {
        errors.password = true;
        formOk = false;
    }

    if (!formData.username) {
        errors.username = true;
        formOk = false;
    }

    return [errors, formOk];
}

export function validateFormLogin(formData) {
    let errorsLogin = {};
    let formOk = true;

    if (!validateEmail(formData.email)) {
        errorsLogin.email = true;
        formOk = false;
    }

    if (formData.password.length < 1) {
        errorsLogin.password = true;
        formOk = false;
    }

    return [errorsLogin, formOk];
}