// Import the functions you need from the SDKs you need
import {initializeApp} from 'firebase/app';
import {getFirestore} from "firebase/firestore";

const firebaseConfig = {
    apiKey: "AIzaSyAm0Hd37L8u1nnykl16T68xPfb6w_Sn93E",
    authDomain: "songfy-1e8c3.firebaseapp.com",
    projectId: "songfy-1e8c3",
    storageBucket: "songfy-1e8c3.appspot.com",
    messagingSenderId: "572833173126",
    appId: "1:572833173126:web:6dd729ee1a7328f93fcb07"
};

initializeApp(firebaseConfig);

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

export {
    db,
    // firebase
}